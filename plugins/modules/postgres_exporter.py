ANSIBLE_METADATA = {
    "metadata_version": "0.1",
    "status": ["preview"],
    "supported_by": "community",  # XXX
}

DOCUMENTATION = """
---
module: postgres_exporter

short_description: Manage Prometheus postgres_exporter for a PostgreSQL instance.

description:
- "Manage Prometheus postgres_exporter for a PostgreSQL instance"

extends_documentation_fragment:
- dalibo.pglift.postgres_exporter_doc
"""

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.dalibo.pglift.plugins.module_utils.argspec import (
    argspec_from_cli,
    exec_apply_cmd,
)
from ansible_collections.dalibo.pglift.plugins.module_utils.versioncheck import (
    check_pglift_version,
)


def run_module() -> None:
    argspec = argspec_from_cli("postgres_exporter")["options"]
    module = AnsibleModule(argument_spec=argspec, supports_check_mode=True)

    check_pglift_version(module)

    if module.check_mode:
        module.exit_json(changed=False)

    exec_res = exec_apply_cmd("postgres_exporter", module)
    module.exit_json(changed=(exec_res["change_state"] is not None), **exec_res)


def main() -> None:
    run_module()


if __name__ == "__main__":
    main()
