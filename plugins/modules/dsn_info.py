ANSIBLE_METADATA = {
    "metadata_version": "0.1",
    "status": ["preview"],
    "supported_by": "community",  # XXX
}

DOCUMENTATION = """
---
module: dsn_info

short_description: get libpq environment variables to connect to a PostgreSQL server instance

description:
- "Get libpq environment variables to connect to a PostgreSQL server instance"

extends_documentation_fragment:
- dalibo.pglift.dsn_info_doc

author:
- Dalibo (@dalibo)
"""


RETURN = """
PGHOST:
  description: instance host or socket directory
  type: str
  returned: always
PGPORT:
  description: instance port
  type: str
  returned: always
PGUSER:
  description: instance user
  type: str
  returned: always
PGPASSFILE:
  description: instance passfile
  type: str
  returned: always
PATH:
  description: original PATH updated with PostgreSQL version binaries path
  type: str
  returned: always
"""
import json

from ansible.module_utils.basic import AnsibleModule
from ansible_collections.dalibo.pglift.plugins.module_utils.argspec import (
    argspec_from_cli,
)
from ansible_collections.dalibo.pglift.plugins.module_utils.versioncheck import (
    check_pglift_version,
)


def run_module() -> None:

    argspec = argspec_from_cli(
        obj_name="instance", include_only=["name", "version", "description"]
    )["options"]

    module = AnsibleModule(argspec, supports_check_mode=True)

    check_pglift_version(module)

    if module.check_mode:
        module.exit_json(changed=False)

    instance_name = module.params["name"]
    if module.params["version"] is not None:
        instance_name = "{version}/{name}".format(
            version=module.params["version"], name=instance_name
        )

    _, stdout, _ = module.run_command(
        [
            "pglift",
            "instance",
            "env",
            instance_name,
            "--output-format=json",
        ],
        check_rc=True,
    )
    env = json.loads(stdout)
    module.exit_json(**env)


def main() -> None:
    run_module()


if __name__ == "__main__":
    main()
