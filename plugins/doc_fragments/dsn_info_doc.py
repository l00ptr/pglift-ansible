from pathlib import Path

from ansible_collections.dalibo.pglift.plugins.module_utils.argspec import build_doc


class ModuleDocFragment(object):
    DOCUMENTATION = build_doc(
        Path(__file__).parent / "instance.json",
        include_only=("name", "version", "description"),
    )
