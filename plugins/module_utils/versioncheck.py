from ansible.module_utils.basic import AnsibleModule
from ansible_collections.dalibo.pglift.plugins.module_utils.importcheck import (
    check_required_libs,
)

PGLIFT_REQUIRED_MIN_VERSION = "0.27.0"
try:
    from ansible.module_utils.compat.version import LooseVersion as Version
except ImportError:
    with check_required_libs():
        from packaging.version import Version


def check_pglift_version(module: AnsibleModule) -> None:
    _, stdout, _ = module.run_command(["pglift", "--version"], check_rc=True)
    v = stdout.split(" ")[-1]
    if Version(v) < Version(PGLIFT_REQUIRED_MIN_VERSION):
        module.fail_json(
            msg=f"Version of pglift ({v}) lower than recommended version ({PGLIFT_REQUIRED_MIN_VERSION})"
        )
